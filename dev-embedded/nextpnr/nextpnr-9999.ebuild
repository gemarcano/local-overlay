# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit git-r3 cmake python-single-r1

DESCRIPTION="nextpnr -- a portable FPGA place and route tool"
HOMEPAGE="https://github.com/YosysHQ/nextpnr"
if [ ${PV} != 9999 ]; then
	SRC_URI=""
else
	EGIT_REPO_URI="https://github.com/YosysHQ/nextpnr.git"
fi

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="${PYTHON_DEPS}
	dev-qt/qtwidgets
	dev-qt/qtcore
	dev-qt/qtwidgets
	>=dev-libs/boost-1.78.0[python]
	dev-cpp/eigen
	sys-libs/zlib
	dev-libs/expat
	virtual/opengl
	sci-electronics/yosys
	dev-embedded/icestorm"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		-DARCH=ice40
		-DICEBOX_ROOT="${EPREFIX}/usr/share/icebox"
	)

	cmake_src_configure
}
