# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..12} )
inherit git-r3 distutils-r1

DESCRIPTION="A tool to parse, extract, and builds 3DS firmware files"
HOMEPAGE="https://github.com/TuxSH/firmtool.git"
if [ ${PV} != 9999 ]; then
	SRC_URI=""
else
	EGIT_REPO_URI="https://github.com/TuxSH/firmtool.git"
fi

LICENSE="BSD"
SLOT="0"
KEYWORDS=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"
DEPEND="
	${PYTHON_DEPS}
	dev-python/pycryptodome[${PYTHON_USEDEP}]
"

RDEPEND="${DEPEND}"
BDEPEND="${RDEPEND}"
