# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit git-r3 python-single-r1

DESCRIPTION="Tools for analyzing and creating bistream files for the Lattice iCE40 FPGA."
HOMEPAGE="http://www.clifford.at/icestorm/"
if [ ${PV} != 9999 ]; then
	SRC_URI=""
else
	EGIT_REPO_URI="https://github.com/cliffordwolf/icestorm.git"
fi

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

PATCHES=(
	"${FILESDIR}/${P}-fix-build-compiler.patch"
)

DEPEND="${PYTHON_DEPS}
	dev-embedded/libftdi:1
	dev-lang/tcl
	dev-libs/libffi
	media-gfx/xdot
	dev-cpp/eigen"
RDEPEND="${DEPEND}"
BDEPEND="sys-devel/bison
	sys-devel/flex"

src_configure() {
	default
	export PREFIX=${EPREFIX}/usr
}
