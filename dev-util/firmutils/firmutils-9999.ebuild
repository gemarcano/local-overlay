# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

if [[ ${PV} == "9999" ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/gemarcano/firmutils.git"
else
	SRC_URI="${P}.tar.xz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="Nintendo 3DS FIRM library and utilities."
HOMEPAGE="https://gitlab.com/gemarcano/firmutils"

LICENSE="|| ( LGPL-2.1+ GPL-2+ )"
SLOT="0"
IUSE="static-libs doc test"
RESTRICT="!test? ( test )"

DEPEND="dev-libs/elfutils
	dev-libs/openssl"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}
	doc? ( app-doc/doxygen )
	dev-libs/cxxopts
	dev-cpp/catch:0"

#src_install()
#{
#	if use doc; then
#		HTML_DOCS+=( "${S}"/documentation/html/* )
#		DOCS+=( README.md )
#		doman "${S}"/documentation/man/man3/*.3
#	fi
#	default
#	find "${D}" -name '*.la' -delete || die
#}
