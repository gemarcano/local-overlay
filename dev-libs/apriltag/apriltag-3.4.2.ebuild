# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="AprilTag is a visual fiducial system popular for robotics research."
HOMEPAGE="https://april.eecs.umich.edu/software/apriltag"
SRC_URI="https://github.com/AprilRobotics/${PN}/archive/refs/tags/v${PV}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="opencv"

DEPEND="opencv? ( media-libs/opencv )"
RDEPEND="${DEPEND}"
BDEPEND=""
