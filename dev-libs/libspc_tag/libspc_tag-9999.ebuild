# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

if [[ ${PV} == "9999" ]] ; then
	inherit git-r3 autotools
	EGIT_REPO_URI="https://gitlab.com/gemarcano/libspc_tag.git"
else
	SRC_URI="${P}.tar.xz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="SNES SPC file format tag library and utilities."
HOMEPAGE="https://gitlab.com/gemarcano/libspc_tag"

LICENSE="|| ( LGPL-2.1+ GPL-2+ )"
SLOT="0"
IUSE="static-libs doc"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="doc? ( app-doc/doxygen )"

if [[ ${PV} == "9999" ]] ; then
	src_prepare()
	{
		default
		eautoreconf
	}
fi

src_configure()
{
	econf $(use_enable static-libs static)
}

src_compile()
{
	default
	if use doc; then
		doxygen "${S}/Doxyfile"
	fi
}

src_install()
{
	if use doc; then
		HTML_DOCS+=( "${S}"/documentation/html/* )
		DOCS+=( README.md )
		doman "${S}"/documentation/man/man3/*.3
	fi
	default
	find "${D}" -name '*.la' -delete || die
}
