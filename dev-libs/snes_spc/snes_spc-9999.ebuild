# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/gemarcano/${PN}.git"
else
	SRC_URI="https://gitlab.com/gemarcano/snes_spc/-/archive/${PV}/${P}.tar.bz2"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="Super Nintendo (SNES) SPC-700 APU emulator and S-DSP emulators."
HOMEPAGE="http://blargg.8bitalley.com/libs/audio.html#snes_spc
	https://gitlab.com/gemarcano/snes_spc"

LICENSE="LGPL-2.1+"
SLOT="0"
IUSE="static-libs"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare()
{
	eautoreconf
	default
}

src_configure()
{
	econf $(use_enable static-libs static)
}

src_install()
{
	default
	find "${D}" -name '*.la' -delete || die
}
