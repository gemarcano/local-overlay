# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION="Super Nintendo (SNES) SPC-700 APU emulator and S-DSP emulators."
HOMEPAGE="http://blargg.8bitalley.com/libs/audio.html#snes_spc"
SRC_URI="https://gitlab.com/gemarcano/snes_spc/-/archive/${PV}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}"/${P}-add-pitch-offset.patch
)

src_prepare()
{
	cp "${FILESDIR}"/{configure.ac,Makefile.am} "${S}"/ || die
	eautoreconf
	default
}

src_configure()
{
	econf $(use_enable static-libs static)
}

src_install()
{
	default
	find "${D}" -name '*.la' -delete || die
}
