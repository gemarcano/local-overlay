# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# NB: The ${PV} tracks the *repo launcher version*, not the last signed release
# of the repo project.  The launcher only gets a new update when changes are
# made in it.

EAPI="7"

PYTHON_COMPAT=( python3_{10..12} )

inherit bash-completion-r1 python-r1

DESCRIPTION="Google tool for managing git, particularly multiple repos"
HOMEPAGE="https://gerrit.googlesource.com/git-repo"
SRC_URI="https://gerrit.googlesource.com/git-repo/+archive/1e9f7b9e9ef473305d10a26a48138bc6ad38ccf6.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86"
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
	!app-admin/radmind
"

S="${WORKDIR}"

src_install() {
	python_foreach_impl python_newscript "${S}/${PN}" ${PN}
	newbashcomp completion.bash ${PN}
	doman man/*.[0-9]
}
