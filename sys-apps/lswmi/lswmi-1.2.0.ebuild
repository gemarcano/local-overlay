# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=hatchling
PYTHON_COMPAT=( python3_{{11..13},13t} )
inherit distutils-r1

DESCRIPTION="Utility to retrieve information about WMI devices on Linux"
HOMEPAGE="
	https://github.com/Wer-Wolf/lswmi
	https://pypi.org/project/lswmi/
"
SRC_URI="
	https://github.com/Wer-Wolf/${PN}/archive/refs/tags/v${PV}.tar.gz
		-> ${P}.gh.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

distutils_enable_tests unittest
