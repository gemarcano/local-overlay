# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils

if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/gemarcano/skyscraper.git"
	EGIT_BRANCH="qt6"
else
	# FIXME
	SRC_URI="https://gitlab.com/es-de/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"
	KEYWORDS=""
fi

DESCRIPTION="Powerful and versatile game data scraper written in Qt and C++."
HOMEPAGE="https://gemba.github.io/skyscraper/"

LICENSE="GPL-3"
SLOT="0"

IUSE="qt6"

DEPEND="
	qt6? (
		dev-qt/qtbase:6[xml,network]
	)
	!qt6? (
		dev-qt/qtcore:5
		dev-qt/qtxml:5
		dev-qt/qtnetwork:5
	)
"

RDEPEND="${DEPEND}"
BDEPEND="
	dev-build/cmake
	qt6? (
		dev-qt/qtbase:6
	)
	!qt6? (
		dev-qt/qtcore:5
	)
"

src_configure() {
	local qmake_args=(
	)
	if use qt6; then
		qmake6 -set PREFIX "${EPREFIX}/usr"
		eqmake6 "${qmake_args[@]//$'\n'}"
	else
		qmake5 -set PREFIX "${EPREFIX}/usr"
		eqmake5 "${qmake_args[@]//$'\n'}"
	fi
}

src_install() {
	emake install INSTALL_ROOT="${D}"
	einstalldocs
}
