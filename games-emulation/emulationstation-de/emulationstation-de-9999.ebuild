# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake xdg-utils

if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/es-de/emulationstation-de.git"
else
	SRC_URI="https://gitlab.com/es-de/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"
	KEYWORDS=""
fi

DESCRIPTION="Frontend for browsing and launching games from your multi-platform collection."
HOMEPAGE="https://es-de.org/"

LICENSE="MIT"
SLOT="0"

IUSE="cec"

PATCHES=(
	"${FILESDIR}"/${PN}-fix-unicode-string-comparison.patch
	"${FILESDIR}"/${PN}-remove-vendored-libraries.patch
	"${FILESDIR}"/${PN}-dont-strip-binaries.patch
	"${FILESDIR}"/${PN}-dont-compress-manpage.patch
	"${FILESDIR}"/${PN}-fix-missing-git-include.patch
)

DEPEND="
	virtual/opengl
	net-misc/curl
	media-video/ffmpeg
	media-libs/freeimage
	media-libs/freetype
	media-libs/harfbuzz
	dev-libs/icu
	virtual/libintl
	>=dev-libs/libgit2-1.9.0
	dev-libs/pugixml
	media-libs/libsdl2
	cec? ( dev-libs/libcec )
	media-libs/alsa-lib
	app-text/poppler
	media-libs/glm
	media-libs/cimg
	dev-libs/rapidjson
	media-libs/rlottie
	dev-libs/utfcpp
	"

RDEPEND="${DEPEND}"
BDEPEND="dev-build/cmake"

src_prepare() {
	rm -r "${S}"/external/CImg
	rm -r "${S}"/external/glm
	rm -r "${S}"/external/rapidjson
	rm -r "${S}"/external/rlottie
	rm -r "${S}"/external/utfcpp
	rm "${S}"/es-app/assets/es-de.6.gz
	cmake_src_prepare
}

src_compile() {
	cmake_src_compile
	cd "${S}"/tools
	sh ./generate_man_page.sh
	cd -
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
