# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit pam autotools

DESCRIPTION="Poldi is a PAM module implementing authentication via OpenPGP smartcards."
HOMEPAGE="http://www.gnupg.org/"
SRC_URI="mirror://gnupg/alpha/poldi/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="nls
	+localdb
	+largefile
	usb
	+x509"

DEPEND="dev-libs/libgcrypt
	sys-libs/pam
	>=dev-libs/libgpg-error-0.7"
RDEPEND="${DEPEND}"
BDEPEND=""
PATCHES=( "${FILESDIR}"/${P}-fix-tests.patch )

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	econf --with-pam-module-directory="$(getpam_mod_dir)" \
		--enable-maintainer-mode \
		--disable-rpath \
		$(use_enable largefile) \
		$(use_enable localdb localdb-auth) \
		$(use_enable x509 x509-auth)
}

src_install() {
	dodoc AUTHORS NEWS README THANKS ChangeLog
	dodir /etc/poldi
	cp "${FILESDIR}"/poldi.conf.example "${ED}"/etc/poldi
	default
}
