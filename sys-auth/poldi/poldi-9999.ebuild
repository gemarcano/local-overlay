# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit pam autotools

if [ ${PV} != 9999 ]; then
	SRC_URI="mirror://gnupg/alpha/poldi/${P}.tar.bz2"
	KEYWORDS="~amd64"
else
	inherit git-r3
	EGIT_REPO_URI="https://github.com/gpg/poldi.git"
fi

DESCRIPTION="Poldi is a PAM module implementing authentication via OpenPGP smartcards."
HOMEPAGE="http://www.gnupg.org/"

LICENSE="GPL-3"
SLOT="0"
IUSE="nls
	+localdb
	+largefile
	usb
	+x509"

DEPEND="dev-libs/libgcrypt
	sys-libs/pam
	>=dev-libs/libgpg-error-0.7"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	econf --with-pam-module-directory="$(getpam_mod_dir)" \
		--enable-maintainer-mode \
		--disable-rpath \
		$(use_enable largefile) \
		$(use_enable localdb localdb-auth) \
		$(use_enable x509 x509-auth)
}

DOCS=( "${S}"/{AUTHORS,NEWS,README,THANKS,ChangeLog} )

src_install() {
	default
	if use localdb; then
		keepdir /etc/poldi/localdb/keys
		touch "${D}"/etc/poldi/localdb/users
	else
		dodir /etc/poldi
	fi
	touch "${D}"/etc/poldi/poldi.conf
}
