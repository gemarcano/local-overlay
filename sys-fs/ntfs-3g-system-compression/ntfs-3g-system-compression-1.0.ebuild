# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION='NTFS-3G plugin for reading "system compressed" files'
HOMEPAGE="https://github.com/ebiggers/ntfs-3g-system-compression"
SRC_URI="https://github.com/ebiggers/${PN}/releases/download/v${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=sys-fs/ntfs3g-2017.3.23"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
	eautoreconf
}
